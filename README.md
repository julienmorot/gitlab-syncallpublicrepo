# gitlab-syncallpublicrepo

## Description

Tiny script to get a local copy of all my public repository on Gitlab.
Only work with public repository I own by design.
I have some repositories on gitlab.com I don't use anymore and the only copy of the code reside on gitlab.
Thus I wanted a small script to make some sort of local backup without having to manually add repo in a backup shell script.

## Usage

Change username and local backup path in the script. I didn't manage a getopt system.
